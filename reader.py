#!/usr/bin/env python3

"""
thing to write file
"""

from argparse import ArgumentParser as Parser
import uproot


def get_args():
    parser = Parser(description=__doc__)
    parser.add_argument('input_file')
    return parser.parse_args()

def run():
    args = get_args()
    tree = uproot.open(args.input_file)['outtree']
    keys = tree.keys()

    # load in the arrays
    arrays = {}
    for k in keys:
        branch = tree[k]
        # you can change the stop and start point, i.e. you can read
        # only one entry if you want (it will be slow though)
        arrays[k] = branch.array(entrystart=0, entrystop=10)

    # now print some stuff
    for key, ar in arrays.items():
        print(key, ar)


if __name__ == '__main__':
    run()
